<?php

use LightSource\FrontBlocks\{
    Renderer,
    Settings
};
use LightSource\FrontBlocksSample\{
    Article\Article,
    Header\Header
};

require_once __DIR__ . '/vendors/vendor/autoload.php';

//// settings

ini_set('display_errors', 1);

$settings = new Settings();
$settings->addBlocksFolder('LightSource\FrontBlocksSample', __DIR__ . '/Blocks');
$settings->setErrorCallback(
    function (array $errors) {
        // todo log or any other actions
        echo '<pre>' . print_r($errors, true) . '</pre>';
    }
);
$renderer = new Renderer($settings);

//// usage

$header = new Header();
$header->loadByTest();

$article = new Article();
$article->loadByTest();

$content = $renderer->render($header);
$content .= $renderer->render($article);
$css     = $renderer->getUsedResources('.css', true);

//// html

?>
<html>

<head>

    <title>Example</title>
    <style>
        <?= $css ?>
    </style>
    <style>
        .article {
            margin-top: 10px;
        }
    </style>

</head>

<body>

<?= $content ?>

</body>

</html>
