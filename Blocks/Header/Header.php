<?php

namespace LightSource\FrontBlocksSample\Header;

use LightSource\FrontBlocks\Block;

class Header extends Block
{

    protected string $name;

    public function loadByTest()
    {
        parent::load();
        $this->name = 'I\'m Header';
    }
}
