<?php

namespace LightSource\FrontBlocksSample\Article;

use LightSource\FrontBlocks\Block;
use LightSource\FrontBlocksSample\Button\Button;

class Article extends Block
{

    protected string $name;
    protected Button $button;

    public function loadByTest()
    {
        parent::load();
        $this->name = 'I\'m Article, I contain another block';
        $this->button->loadByTest();
    }
}
