<?php

namespace LightSource\FrontBlocksSample\Button;

use LightSource\FrontBlocks\Block;

class Button extends Block
{

    protected string $name;

    public function loadByTest()
    {
        parent::load();
        $this->name = 'I\'m Button';
    }
}
